const SecureMesh = require("../lib.js");
const Hub = require("signalhub");
const {dialog,app} = require("electron").remote
const path = require("path")
function change_tab(tab,pm,nick) {
  let current_history = document.getElementById(current_channel.id);
  current_history.style.display = "none";
  current_history.className = "";
  document.getElementById(current_channel.id + "-listing").classList.remove('active');
  current_channel = {
    id: tab,
    pm: false,
    nick: nick
  };
  let new_tab = document.getElementById(tab);
  if (new_tab != undefined) {
    new_tab.style.display = "block";
    new_tab.className = "message-history col";
    document.getElementById(tab + "-listing").classList.add('active');
  } else {
    let tab_listing = document.createElement('li');
    tab_listing.innerHTML = `<li class="channel active" id="${tab}-listing"><a class="channel_name"><span class="prefix">${pm ? "@" : ""}</span><span>${pm ? nick : tab}</span></a></li>`;
    tab_listing.onclick = function () {
      change_tab(tab,pm,nick);
    };
    document.getElementById(pm ? "pm_list" : "channel_list").appendChild(tab_listing);
    let new_history = document.createElement('div');
    new_history.innerHTML = `<div class="message-history col" id="${tab}" style="width: 100%;"></div>`;
    document.getElementById("mainrow").appendChild(new_history);
  }
}

let mesh;
let current_channel = {
  id: "server",
  pm: false,
  nick: ""
};

document.getElementById("server-listing").onclick = function () {
  change_tab("server",false,"");
};

let first_form = document.getElementById("form1");
first_form.addEventListener("submit", function (event) {
  event.preventDefault();
  document.getElementById("first").style.display = "none";

  mesh = new SecureMesh(
    new Hub(document.getElementById("room").value,document.getElementById("hub").value.split(',')),
    {keys: [document.getElementById("key").value]},
    {nick:document.getElementById("nick").value}
  );

  let second_form = document.getElementById("form2");
  let messageinput = document.getElementById("messageinput");
//  let messages = document.getElementById("history");
  let files = document.getElementById("file-select");

  document.getElementById("second").style.display = "block";
  document.getElementById("footer").style.display = "inline";

  mesh.on("header", function(data,id) {
    console.log(data);
    if (data.type == 'textmsg') {
      let m_l = document.createElement("div");
      let date = new Date();
      m_l.innerHTML = `<div class="message" id="${data.id}" style="width:100%"><a class="message_profile-pic" href=""></a><a class="message_username" href="">${mesh.nicks[id]}</a><span class="message_timestamp">${date.getHours()}:${date.getMinutes()}</span><span class="message_star"></span><span class="message_content" id="${data.id}-content"></span></div>`;
  		document.getElementById(data.channel).append(m_l);
  	} else if (data.type == 'filemsg') {
      let m_l = document.createElement("div");
      let date = new Date();
      m_l.innerHTML = `<div class="message" id="${data.id}" style="width:100%"><a class="message_profile-pic" href=""></a><a class="message_username" href="">${mesh.nicks[id]}</a><span class="message_timestamp">${date.getHours()}:${date.getMinutes()}</span><span class="message_star"></span><span class="message_content" id="${data.id}-content"><i class="fas fa-spinner"></i><a class="downloada" onclick="console.log('hey')">${data.name}</a></span></div>`;
      m_l.querySelector(".downloada").onclick = function() {
        let defaultpath = "";
        try {
          defaultpath = app.getPath("downloads")
        } catch {
          defaultpath = app.getPath("home")
        }

        let savepath = dialog.showSaveDialog({defaultPath: path.join(defaultpath,data.name)});

      	mesh.request_file(data.id,savepath,id);
      };
      document.getElementById(data.channel).append(m_l);
    }
  });

  mesh.on("data", function (data,id) {
    console.log(data);
    if (data.type == "text") {
      document.getElementById(`${data.id}-content`).innerHTML = data.data;
    }
  });

  mesh.on("uploading", function (fid) {
    console.log(fid);
    let message = document.getElementById(`${fid}-content`);
    let measure = mesh.files_sent[fid].size >= 1000000 ? "mb" : "kb";
    let size_adjusted = Math.round(measure == "mb" ? mesh.files_sent[fid] / 1000000 : mesh.files_sent[fid] / 1000);
    message.insertAdjacentHTML('beforeend',`
    <div class="progress" style="width: 22em; margin-top: 2%;">
      <div class="progress-bar progress-bar-striped" id="${fid}-progress" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0${measure}/${size_adjusted}${measure} (0%)</div>
    </div>
    `);
  });

  mesh.on("uploadprogress",function(event) {
    let message = document.getElementById(`${event.fid}-progress`);
    let measure = mesh.files_sent[event.fid].size >= 1000000 ? "mb" : "kb";
    let size_adjusted = measure == "mb" ? mesh.files_sent[event.fid].size / 1000000 : mesh.files_sent[event.fid].size / 1000;
    let progress_adjusted = Math.round(measure == "mb" ? event.progress / 1000000 : event.progress / 1000);
    let percent = Math.round((event.progress / mesh.files_sent[event.fid].size) * 100).toString();
    message.innerHTML = `${progress_adjusted}${measure}/${size_adjusted}${measure} (${percent}%)`
    message.setAttribute("aria-valuenow",percent)
    message.setAttribute("style","width:" + percent + "%")
  });

  mesh.on("downloading", function (fid) {
    let message = document.getElementById(`${fid}-content`);
    let measure = mesh.files_received[fid].size >= 1000000 ? "mb" : "kb";
    let size_adjusted = measure == "mb" ? mesh.files_received[fid] / 1000000 : mesh.files_received[fid] / 1000;
    message.insertAdjacentHTML('beforeend',`
    <div class="progress" style="width: 22em; margin-top: 2%;">
      <div class="progress-bar progress-bar-striped bg-success" id="${fid}-progress" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0${measure}/${size_adjusted}${measure}</div>
    </div>
    `);
  });

  mesh.on("downloadprogress",function(event) {
    let message = document.getElementById(`${event.fid}-progress`);
    let measure = mesh.files_received[event.fid].size >= 1000000 ? "mb" : "kb";
    let size_adjusted = measure == "mb" ? mesh.files_received[event.fid].size / 1000000 : mesh.files_received[event.fid].size / 1000;
    let progress_adjusted = Math.round(measure == "mb" ? event.progress / 1000000 : event.progress / 1000);
    let percent = Math.round((event.progress / mesh.files_received[event.fid].size) * 100).toString();
    message.innerHTML = `${progress_adjusted}${measure}/${size_adjusted}${measure} (${percent}%)`
    message.setAttribute("aria-valuenow",percent)
    message.setAttribute("style","width:" + percent + "%")
  });

  mesh.on("stream",function (stream,id) {
    let audios = document.getElementById("audios");
    let audiosource = document.createElement("audio");
    audiosource.src = window.URL.createObjectURL(stream);
    audios.appendChild(audiosource);
  })

  messageinput.addEventListener("keydown",function(event) {
    console.log("hey?");
    if (event.key == 'Enter') {
      let val = messageinput.value;
      if (val.charAt(0) == '/') {
        let split = val.split(' ');
        switch (split[0]) {
          case '/join':
            mesh.join(split[1]);
            console.log('owo!');
            change_tab(split[1]);
            break;
          case '/joinvc':
            navigator.getUserMedia({ video: false, audio: true }, function(stream) {
              mesh.joinvc(split[1],stream);
            }, function () {});
            break;
          default:
        }
      } else {
        mesh.say({ header: { type: 'textmsg' }, data: { data: messageinput.value, type: 'text'} },current_channel.id);

        let m_l = document.createElement("div");
        let date = new Date();
        m_l.innerHTML = `<div class="message"><a class="message_profile-pic" href=""></a><a class="message_username" href="">${mesh.nick}</a><span class="message_timestamp">${date.getHours()}:${date.getMinutes()}</span><span class="message_star"></span><span class="message_content">${messageinput.value}</span></div>`;
    		document.getElementById(current_channel.id).appendChild(m_l);


      }
      messageinput.value = "";
    }
  });

  files.addEventListener("change", function(event) {
      for (f of files.files) {
         let id = mesh.say_file(f,current_channel.id);
         let m_l = document.createElement("div");
         let date = new Date();
      //    if (f.type.split('/')[0] == 'image') {
          //  m_l.innerHTML = `<div class="message container"><a class="message_profile-pic" href=""></a><a class="message_username" href="">${mesh.nick}</a><span class="message_timestamp">${date.getHours()}:${date.getMinutes()}</span><span class="message_star"></span><span class="message_content"><img src="${reader.result}" class="img-thumbnail rounded"></img></span></div>`;
         m_l.innerHTML = `<div class="message" id="${id}"><a class="message_profile-pic" href=""></a><a class="message_username" href="">${mesh.nick}</a><span class="message_timestamp">${date.getHours()}:${date.getMinutes()}</span><span class="message_star"></span><span class="message_content" id="${id}-content"><i class="fas fa-file"></i> <a>${f.name}</a></span></div>`;
         document.getElementById(current_channel.id).appendChild(m_l);
     };
  });
});
