const Swarm = require('secure-webrtc-swarm')
const Hub = require('signalhub')
const EventEmitter = require('events')
const {Readable,Transform} = require('stream')
const uuid = require('uuid/v1')
const JSONStream = require('JSONStream')
const zlib = require('zlib')
const fs = require('fs')

class SecureMesh extends EventEmitter {
	constructor(hub,swarmopts,opts) {
		super()
		this.swarm = new Swarm(hub,swarmopts)
		this.nick = opts.nick
		this.nicks = {}
		this.channels = {} // channel name: peeer ids
		this.streams = {}
		this.files_sent = {}
		this.files_received = {}
		this.files_downloading = {}

		let that = this

		this.swarm.on('peer',function (peer,id) {

			peer._pc.ondatachannel = function(event) {
				console.log(event)
				if (event.channel.label.substring(0,5) == "file-") {
					let fid = event.channel.label.substring(5)
					if (fid in that.files_downloading) {
						that.emit("downloading",fid)

						let decompressor = new zlib.Gunzip()
						decompressor.pipe(that.files_downloading[fid])

						let progress = 0
						decompressor.on('data', function(d) {
							progress += d.length
							that.emit('downloadprogress',{fid: fid, progress:progress})
						})

						event.channel.onmessage = function (d) {
							console.log('received stuff')
							decompressor.push(Buffer.from(d.data))
						}
					}
				} else {
					peer._setupData(event)
				}
			}

			console.log('delete my life')
			that.streams[id] = new zlib.Gzip()
			that.streams[id].pipe(peer)

			let unzipper = new zlib.Gunzip()
			let stream = JSONStream.parse('*')
			peer.on('data', function(d) {
				unzipper.push(d)
			});
			unzipper.on('data', function(d) {
				console.log(d.toString())
			});
			unzipper.pipe(stream)

			stream.on('data', function(stuff) {
				console.log(stuff)
				if (stuff.ish) {
					console.log(stuff)
					switch (stuff.type) {
						case 'nick':
							that.nicks[id] = stuff.nick
							break
						case 'join':
							if (stuff.channel in that.channels) {
								that.pm({header: {type: 'joinresponse', channel: stuff.channel} },id)
								that.channels[stuff.channel].push(id)
							}
							break
						case 'joinresponse':
							that.channels[stuff.channel].push(id)
							break
						case 'joinvc':
							if (stuff.channel in that.channels) {
								that.pm({header: {type: 'vcresponse', channel: stuff.channel}},id)
								that.channels[stuff.channel].push(id)
								peer.addStream(this.media)
							}
							break
						case 'vcresponse':
							that.channels[stuff.channel].push(id)
							peer.addStream(this.media)
							break
						case 'exitvc':
							that.channels[stuff.channel].splice(that.channels[stuff.channel].indexOf(id),1)
							peer.removeStream(this.media)
							break
						case 'filemsg':
							that.files_received[stuff.id] = stuff
							break
						case 'filereq':
							if (stuff.fid in that.files_sent) {
								that.emit("uploading",stuff.fid)

								let datac = peer._pc.createDataChannel(`file-${stuff.fid}`)

								let file = fs.createReadStream(that.files_sent[stuff.fid].path)
								let compressor = new zlib.Gzip()
								file.pipe(compressor)

								let progress = 0
								file.on('data', function(d) {
									progress += d.length
									that.emit('uploadprogress',{fid: stuff.fid,progress: progress})
								})

								compressor.on('data', function(d) {
									console.log('sent stuff')

									datac.send(d)
								})
							}
							break
						default:
					}
					that.emit('header',stuff,id)
				} else {
					that.emit('data',stuff,id)
				}
			})

			peer.on('stream',function(stream)  {
				that.emit("stream",stream,id)
			})

			stream.on('header', function (header) {
				console.log("header as in cut my head off")
				console.log(header)
			})

			that.pm({header: {type:'nick',nick: that.nick}},id)

		})
	}

	broadcast (message) {
			for (let p of Object.keys(this.swarm.remotes)) {
				console.log(p)
				console.log(this.streams)
				this.streams[p].push(message)
			}
	}

	say (message,channel) {
		if (channel in this.channels) {
			let id = uuid()

			message.header.date = new Date().getTime()
			message.header.id = id
			message.header.channel = channel
			message.header.ish = true

			if (message.data) {
				message.data.id = id
				message.data.ish = false
			}

	 		for (let p of this.channels[channel]) {
				this.streams[p].push(JSON.stringify(message))
			}
		}
	}

	say_file(file,channel) {
		if(channel in this.channels) {
			let message = {
				header: {
					type: 'filemsg',
					date: new Date().getTime(),
					id: uuid(),
					channel: channel,
					name: file.name,
					size: file.size,
					ish: true
				}
			}
			this.files_sent[message.header.id] = file
			let encoded = JSON.stringify(message)

			for (let p of this.channels[channel]) {
				this.streams[p].push(encoded)
			}
			return message.header.id
		}
	}

	request_file(fileid,savepath,sender) {
		this.pm({header: {type:'filereq', fid: fileid}},sender)
		this.files_downloading[fileid] = fs.createWriteStream(savepath)
	}

	pm (message, id) {
		if (id in this.swarm.remotes) {
			let mid = uuid()

			message.header.date = new Date().getTime()
			message.header.id = mid
			message.header.ish = true

			if (message.data) {
				message.data.id = mid
				message.data.ish = false
			}

			this.streams[id].push(JSON.stringify(message));
		}
	}

	join (channel) {
		this.channels[channel] = []
		this.broadcast(JSON.stringify({header :{ ish: true, type: 'join',channel: channel}}))
	}

	joinvc (channel,mediastream) {
		this.channels[channel] = []
		this.media = mediastream
		this.broadcast(JSON.stringify({header :{ ish: true, type: 'joinvc',channel: channel}}))
	}

	exitvc (channel) {
		this.broadcast(JSON.stringify({header: {ish:true, type:'exitvc', channel: channel}}))
	}
}

module.exports = SecureMesh;
