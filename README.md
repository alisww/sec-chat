# Alis' Encrypted P2P chat

Current features:
  - AES Encryption for peer discovery exchange, all messages and files with a shared secret
  - File sharing
  - Channels

Planned features:
  - Asymetric encryption
  - PMs
  - Voice calling

## Build instructions
Run `npm install` to download all dependencies.  
Then run `npm build-web` to bundle the files required for the web client.  
To use the web client, just open up index.html on a browser (Firefox doesn't seem to work right now)  
Otherwise, run the terminal client with `node bin.js`  
